---
title: "How to Participate"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The first step to get involved in the OSPO Alliance is joining our **mailing list**. The archive is public and subscription is free and open to all.

* OSPO Alliance public mailing list: https://accounts.eclipse.org/mailing-list/ospo.zone
* OSPO Alliance mailing list archive: http://www.eclipse.org/lists/ospo.zone

You can also collaborate with the OSPO Alliance on several assets:

- The **OSPO Alliance website**, which is managed as [an open source Eclipse project](https://gitlab.eclipse.org/eclipse/plato/www). The Eclipse Plato project is hosted on the Eclipse GitLab forge, and questions, issues and contributions are welcome. See https://gitlab.eclipse.org/eclipse/plato.
- The **Good Governance Initiative** holds the collaborative work on the GGI Handbook. 
    - Content is [hosted at OW2 gitlab](https://gitlab.ow2.org/ggi/ggi),
    - Public discussions and governance thru [its own mailing list](https://mail.ow2.org/wws/info/ossgovernance) or [forum](https://forum.ospo-alliance.org/),
    - Regular meetings take place every other Monday at 3pm CEST (odd weeks) on [Big Blue Button](https://bbb.opencloud.lu/b/flo-jqs-nir-elb). No registration is required, just connect and join the discussion. Minutes are publicly available in this [Pad folder](https://pad.castalia.camp/mypads/?/mypads/group/ggi-minutes-zz1b0nmd/view),
    - You can also help on [**translating of the handbook**](/translations) to make it more inclusive and accessible to the global community.
- Got a mature OSPO already? We need **case studies** about best practices and, perhaps unexpectedly, not-so-good practices (e.g. things that didn't work out). 
- You can [join our **Task forces**](/taskforces/) to work on other dedicated topics.
- You can **give a talk** during one of our [**OnRamp sessions**](/onramp). Experience feedbacks, methods, tooling, and any topic related to OSPO (Security, HR, Security), are welcome. You can subscribe to the dedicated [OnRamp mailing-list](https://framalistes.org/sympa/info/ospo.onramp).
- **Feedback is important** to us, so please feel free to comment on the GGI [Handbook activities](/ggi/) for specific questions or remarks, or on the mailing lists for more general-purpose discussions. 

Please note that we have, and enforce, a [code of conduct](/code_of_conduct/) to ensure a respectful, inclusive and welcoming community. Some parts of the site, like [OnRamp](/onramp/), also use the [Chatham House rule](https://www.chathamhouse.org/about-us/chatham-house-rule), so please be responsible when quoting people here.
