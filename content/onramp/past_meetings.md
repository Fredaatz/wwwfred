---
title: "OSPO OnRamp past meetings"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The idea of the OSPO OnRamp meeting series is to provide a low-threshold entry point for organisations who want to exchange and learn about the basics on how to set up an OpenSource Program Office and get started into open source.

Please check the [main OnRamp page](/onramp/) for more information about the next meetings.

The meeting are scheduled for every third Friday of the month from 10:30-12:00 CET.

## Past meetings

**Beyond version 1 of the Open Source AI Definition.** — January 17th 2025.

- 🗣 _Speaker_: [**Stefano Maffulli**](https://www.linkedin.com/in/maffulli), Executive Director of the [**Open Source Initiative**](https://opensource.org).
- 📜 _Abstract_: 
   > The release of v.1.0 of the Open Source AI Definition answers a lot of questions and leaves others unanswered. We'll briefly cover the 2+ years of the co-design process that led to the Open Source AI Definition 1.0 and highlight the unanswered questions with suggestions on how to move forward and how OSPOs can leverage it in their open source policies.
- 📊 [View slides](/resources/onramp/20250117_OSPO_OnRamp_OSAID.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1737106004818) [[alternate video](https://media.inno3.eu/w/9Td4UBychbXtcomVNWtYt1)]


**Open Source strategy and governance at the city level: testimony from Echirolles** — December 20th 2024.

- 🗣 _Speaker_: [**Nicolas Vivant**](https://www.linkedin.com/in/vivant/), Director of Digital Strategy at the [**City of Echirolles**](https://www.echirolles.fr/) (France).
- 📜 _Abstract_: 
   > Nicolas Vivant told us the story of the determined transition of the City of Echirolles towards open source software started several years ago, addressing the major issues associated with digital technology, including sovereignty, respect for personal data, environmental impact, cybersecurity, etc. He explained the various steps and methodology implemented: the emergence of the project, the political dimension, the challenges related to management, as well as the practical approach followed, always giving a priority on the final service rendered. Echirolles is also co-organiser of AlpOSS, an annual event held at the town hall that brings together local opensource players.
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1734686796765) [[alternate video](https://media.inno3.eu/w/uiSefpbr2JJXeHLs4E9dtn)]

**OSPO in the Italian public sector: opportunities, challenges and the road ahead** — June 21st 2024.

- 🗣 _Speaker_: **Leonardo Favario** — Head of Open Source Program Office at **[PagoPA S.p.A.](https://www.pagopa.it/)**.
- 📜 _Abstract_:
    > Gain insights from the recently established Open Source Program Office of PagoPA, an Italian public company. This presentation focuses on the inception of the OSPO, a multidisciplinary team responsible for managing all the open source initiatives within the organization. Emphasis will be placed on the development of the company's open source strategy and its alignment with Italian national norms and guidelines. Additionally, the discussion will address current hurdles and outline future directions, providing a comprehensive overview of the challenges and opportunities that lie ahead for the entire Italian public ecosystem.
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1718957947123) [[alternate video](https://media.inno3.eu/w/6JHft9y6FfiqVjnxSon8iH)]

****

**The actions of the free software mission** — May 24th 2024

- 🗣 _Speaker_: **Bastien Guerry** — Head of the Free software mission at **[DINUM](https://www.numerique.gouv.fr/dinum/)**, Inter-Ministerial Directorate of Digital Affairs (French State).
- 📜 _Abstract_:
  > The free software strategy of the interministerial digital direction is based on three objectives to be implemented with all the government departments: use more free software, publish more source code, and attract open source-related skills to the public sector. The presentation will show the means implemented to achieve this mission, and the results obtained.
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1716538849468) [[alternate video](https://media.inno3.eu/w/vjFApudMx6UP4eehHrAm6M)]

****

**How to select the right open source solution? Get inspired by OW2 MRL (Market Readiness Levels)!** — April 19th 2024

- 🗣 Speaker: **Antoine Mottier** — Chief Technology Officer at **[OW2](https://www.ow2.org)**.
- 📜 _Abstract_:
  > Market Readiness Level is an OW2 solution that aims at benchmarking open source projects to ease the adoption of mature solutions and identification of new offering. During this webinar Antoine will give you a tour of MRL. This should allow you to quickly start using it when you need to evaluate an open source software. The conference will also highlight the benefits of the method whether you are involved in the development of a given project or simply as an open source user. We will cover the internal logic of MRL to demystify the MRL score. And finally, we will have a look at the technical architecture of the solution and what we are aiming at for future versions.
- 📊 [View slides](/resources/onramp_2024/20240419_OSPO_OnRamp_MRL.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1713514474233) [[alternate video](https://media.inno3.eu/w/g7p6oKV5Mw6iULVtFRkve9)]

****

**Now more than ever, OSPO alignment with org strategy is key.** — January 19th 2024

- 🗣 Speaker: **Tobie Langel** — Open ecosystem strategy consultant, **[UnlockOpen](https://unlockopen.com/)**. 
- 📜 _Abstract_:
  > 2023 has been brutal for the tech industry. Even more so for OSPOs, which suffered budget and headcount cuts, felt increased pressure around software supply chain security, and saw AI stealing the spotlight. The very tactical approach that worked when credit was cheap and money was flowing no longer cuts it. In today's environment, strategic alignment with organizational mission, goals, and strategy becomes essential to demonstrate value and get executive attention, buy-in, and support. Tobie Langel has been helping organizations think strategically about their open source involvement for close to a decade. He'll be sharing some of the frameworks he uses to align externally-facing teams with internal stakeholders and become more impactful both internally and externally as a result.
- 📊 [View slides](/resources/onramp_2024/20240119_OSPO_OnRamp_ospo_align.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1705656257470) [[alternate video](https://media.inno3.eu/w/91bKjjUWy6YFHJo5Q5cRj1)]

****

**Case  Studies from the Frontiers of Knowledge Transfer in a European University** — December 15th 2023

- 🗣 Speaker: **John Whelan** — Director of OSPO, **[Trinity Innovation, Trinity College](http://www.tcd.ie/innovation)**, University of Dublin. .
- 📜 _Abstract_:
  > Trinity is the first University in Europe to have an official OSPO. However, for many years we have supported open source in our Technology Transfer Office through: 1) Spinning Out Campus Companies with OSS business models, 2) Dual Licensing of Government Funded Researchand 3) Open Source Education and Training for Researchers. These will be illustrated through practical and real case studies over the years.
- 📊 [View slides](/resources/onramp_20231215/20231215_OSPO_OnRamp_TCD_OSPO.pdf)
- 🎥 Video currently unavailable due to a technical issue during the recording

****

**What’s new in version 1.2 of the Good Governance Initiative?** — November 17th 2023, 10:30-12:00

- 🗣 Speaker: **Boris Baldassari** — Chairperson, **[OSPO Alliance](https://ospo-alliance.org/)**. 
- 📜 _Abstract_:
  > Boris Baldassari, Chairperson of the OSPO Alliance & Open Source Expert at the Eclipse Foundation, unveiled the latest version 1.2 of the Good Governance Initiative. His detailed presentation covered new updates such as 1) the relationships between activities ; 2) the InnerSource chapter, and 3) the My GGI Board. This iteration marks significant progress, offering more refined support to organizations of all types.
- 📊 [View slides](/resources/onramp_20231117/20231117_OSPO_OnRamp_GGI-1.2.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1700211741467) [[alternate video](https://media.inno3.eu/w/p45hgX6cqeFcyuxeSJstaw)]

****

**OSPO in the Academic World** ­— October 20th 2023

- 🗣 Speaker: **Anne-Marie Scott** — Board Chair, **[Apereo Foundation](https://www.apereo.org/)**. 
- 📜 _Abstract_:
  > The establishment of OSPOs in higher education has seen a steady increase over the last few years, especially in North America where the support they can offer for open research is increasingly being recognised and funded. However, open education has a much longer history in universities, of which open source software is just one part. Drawing on our experiences within the Apereo community and beyond, this talk will unpack the concept of the “academic OSPO” in that wider open context, and highlight the full breadth of activities an OSPO could support in the academy.
- 📊 [View slides](/resources/onramp_20231020/20231020_OSPO_OnRamp_OSPO_Academic_World.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1697790501307) [[alternate video](https://media.inno3.eu/w/sjqFyPDWT641RKemhxSGNQ)]

****

**Introduction to the Governance game** ­— June 16th 2023

- 🗣 Speaker: **Jan Ainali** — Codebase Steward, **[Foundation for Public Code](https://publiccode.net/)**
- 📜 _Abstract_:
  > Jan will present the Governance Game and how your organisation can use it to create discussions about effective #OpenSource governance. The Governance game is a tool to get a conversation and reflection about governance in a codebase started. It can be used either just to learn about the complexities of collaboratively built codebases in general, or for a community of a specific codebase to get a shared understanding of what current challenges are and explore changes to mitigate them.
- 📊 [View slides](/resources/onramp_20230616/20230616_OSPO_OnRamp_The_Governance_Game.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1686902873481) [[alternate video](https://media.inno3.eu/w/gJDfzGTuPoF6Tofrx9yGGC)]


****

**A Vision of FOSS @Mercedes-Benz** ­— May 26st 2023

- 🗣 Speaker: **Wolfgang Gehring** — FOSS ambassador and OSPO Leader at **[Mercedes-Benz Tech Innovation](https://www.mercedes-benz-techinnovation.com/)**. .
- 📜 _Abstract_: 
  > At Mercedes-Benz, they decided that they don’t want to “just use FOSS”, but to fully embrace all aspects of it. This means they needed a dedicated Open (and Inner) Source strategy and an Open Source Program Office – but first, they had to define what it really means to “embrace FOSS”. Come with our speaker of the month, Wolfgang, on their journey which started about 6 years ago. He'll show you what we learned, what the crucial points were in implementing the strategy, and what they wish they had already known at the beginning.
- 📊 [View slides](/resources/onramp_20230526/20230526_OSPO_OnRamp_Mercedes-Benz_OSPO.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1685090362627) [[alternate video](https://media.inno3.eu/w/s4vCBEt5h35KWeJxY7mXb8)]

****

**Return of experience on Open Source Software & Hardware governance in Thales** ­— April 21st 2023

- 🗣 Speaker: **Sébastien Lejeune** — OSS Community manager at **[Thales Group](https://www.thalesgroup.com/)**.
- 📜 _Abstract_: 
   > Open Source Software & Hardware governance in Thales are major challenges in our R&D and business strategy.
   Today Open Source matters more than ever for multiple reasons: \
   >  - Foster innovation during inception phase
   >  - Impose de facto Standard
   >  - Interoperability to serve core business
   >  - Share cost
   >
   > In Thales we believe that we can leverage Open Source usage through different roles : consumer, user and contributor. Participation to the OSS communities is one way to control our own destiny and community recognition is mandatory to orient the roadmap. Open Source is also a way to enhance our employer branding by creating & structuring the hiring pipeline. We want to develop the employer Branding through the renown of contributors which allows to attract and retain the best talents in the company.
   > 
   > Alliance and partnerships are key, Thales became the first French industrial company to join RISC-V as platinium member in 2018 and we also are OpenHW Group founder to ensure a strong European presence in Open Source ecosystem thanks to our expertise on Open Hardware technologies. 
   > 
   > We definitely need to accelerate our recognition and impact in OSS, therefore we put in place a federated process for all business lines across Thales to allow our collaborators to share assets on our GitHub organization and to contribute to existing Open Source projects. It is an on-going transformation, from an emerging culture to the new industrial standard, from observer to actor which is now impacting our business.


- 📊 [View slides](/resources/onramp_20230421/20230421_OSPO_OnRamp_Thales_OSPO.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1682064908249) [[alternate video](https://media.inno3.eu/w/vHwRKreu5EaBRbn2ojUUit)]

****

**Paris Open Source — The City's commitment to Open Source and roadmap** ­— March 17th 2023

- 🗣 Speaker: **Philippe Bareille** — Open Source Program Officer at the **[City of Paris](https://www.paris.fr/)**.
- 📊 [View slides](/resources/onramp_20230317/20230317_OSPO_OnRamp_Paris_OSPO.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1679045082451) [[alternate video](https://media.inno3.eu/w/haSdkf2V8ugFFNWSZ3ohc1)]

****

**FSFE’s Legal Network and its Legal and Licensing Workshop (LLW)** ­— February 17th 2023

- 🗣 Speaker: **Gabriel Ku Wei Bin** — Legal Coordinator at the **[FSFE](https://fsfe.org/)**.
- 📜 _Abstract_: 
   > The Free Software Foundation Europe (FSFE) manages a Legal Network of experts in different fields involved in Free Software legal issues. With over 400 members from different legal systems, academic backgrounds, and affiliations around the world, its aim is to promote discussion and foster better knowledge of the legal constructs that back Free Software. This talk will discuss how the Legal Network has promoted and sped up recent developments in the legal field relating to Free Software, as well as touch on affiliated events that the FSFE organizes for the Network, including its annual conference.
- 📊 [View slides](/resources/onramp_20230217/20230217_OSPO_OnRamp_FSFE_Legal_Network.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1676624644585) [[alternate video](https://media.inno3.eu/w/sSM7htXHdtfbBoDJ8QiDJ8)]


****

**Open Source Software Supply Chain Security — Why does it matter?** ­— January 20th 2023

- 🗣 Speaker: **Mikael Barbero** - Head of Security at the **[Eclipse Foundation](https://www.eclipse.org/)**.
- 📜 _Abstract_: 
   > Mikael Barbero will review the various threats targeting the Open Source Software Supply Chain that could lead to attacks such as unpatched software vulnerabilities, 0-days, typo-squatting, dependency confusion, impersonation, hypocrite commits, compromise of code repositories, build servers, or package mirrors.
   > 
   > Of course, he will also give an overview of the industry current best practices and the risk mitigation frameworks that emerge. All along the talk, he will provide tips and tricks on how OSPO can help and participate in securing the supply chain of their Open Source Software and concretely what the Eclipse Foundation is doing to help the Eclipse Projects with those issues.
- 📊 [View slides](/resources/onramp_20230120/2023_01_OSPO_OnRamp_Open_Source_Software_Supply_Chain_Security_Why_does_it_matter.pdf)
- 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1674205066957) [[alternate video](https://media.inno3.eu/w/bp3hDMUHm1hPnYFqVAsf9x)]

****

**State of the (OSPO) Alliance: talks, meetings, members, projects progress, and so on** ­— December 16th 2022

- 🗣 Speakers: **Boris Baldassari, Florent Zara** from the **[Eclipse Foundation](https://www.eclipse.org/)**
- 📜 _Abstract_: 
   > During this session we will present a State of the (OSPO) Alliance: talks, meetings, members, projects progress, and so on.  It will be followed by a live demonstration of the brand new GGI deployment feature. Then, we will share the current status of the Roadmap for 2023 and have an informal discussion about the future of OSPO Alliance.
- 📊 [View slides](/resources/onramp_20221216/221216_OSPO_OnRamp_State_of_the_Alliance.pdf)
- 🎥 [Watch video](https://peertube.xwiki.com/w/3rcCUc4Zs4ZD2Dmii5v8rC)

****

**MAIF : a singular open-source strategy** ­— October 21st 2022

- 🗣 Speakers: **Frédéric Pied** and **François Desmier** from **[MAIF](https://maif.fr/)**
- 📜 _Abstract_: 
   > MAIF is the sixth car insurance in France, first to adopt mutualist model in the country in 1934.   This talk presents how we launched and maintain our open-source strategy [#OSSbyMAIF](https://maif.github.io/) since 2017, from governance to lines of code.

- 📊 [View slides][/resources/onramp_20221021/20221021_OSPO_OnRamp_MAIF.pdf]

****

**Hermine project: Open source code and data for legal compliance** ­— September 16th 2022

- 🗣 Speakers: **Camille Moulin** and **Benjamin Jean** from **[InnoCube](https://inno3.fr/)**
- 📜 _Abstract_:
  > [Hermine](https://gitlab.com/hermine-project/hermine) is an Open Source community-driven project to manage SBOMs and the obligations that come from the licences they involve. It has been initiated by Inno³ and 5 partner companies: Enedis, Lectra, Orange, OVHCloud, RTE. \
The goal of the project is to share the code, but also the data, one very important aspect being to help standardise licence interpretations. Technically, it's a Django/Django REST project. It's still in early development stage, but the code is already publicly available.
- 📊 [View slides](/resources/onramp_20220916/20220916_OSPO_OnRamp_Hermine_v03.pdf)

****

**Setting up an OSPO at a power utility** ­— June 17th 2022

- 🗣 Speaker: **Lucian Balea** from **[RTE](https://www.rte-france.com/)**
- 📜 _Abstract_: 
   > In 2018, RTE (the power transmission system operator in France) decided to embrace open source to increase the efficiency and innovation of core-business software projects. Learning from other industries, it became soon obvious that this strategy would require a more structured approach for open source activities, i.e. an OSPO. The presentation will outline the organization and missions of RTE’s OSPO, its role in community building and in the transformation of internal software activities (development practices, culture, procurement). It will also discuss the approach and challenges in the implementation of compliance processes as well as next topics of focus.
- 📊 [View slides](/resources/onramp_20220617/220617_OSPO_OnRamp_Building_an_OSPO_at_RTE.pdf)
- 🎥 [Watch video](https://peertube.xwiki.com/w/bDWqEvU5twosD2J1jpYj3f)

****

**Building an OSPO: 3rd Party Outbound OSPO** ­— May 20th 2022

- 🗣 Speaker: **Josep Prat** from **[Aiven](https://aiven.io/)**
- 📜 _Abstract_: 
   > Learn from Aiven’s experience building their OSPO which focus is on 3rd party Open Source projects. We will share the benefits that the company is experiencing as well as direct examples of how our OSPO is improving the sustainability of various OSS communities. But the roads of our project were seldom perfect, and we'll discuss pitfalls and areas to watch out for, so you know them in advance when building your own Outbound OSPO focusing on 3rd party OSS. The audience will learn:
   > - An Innovative way of doing OSPOs
   > - When it’s good and when it’s not
   > - How it can improve the global OSS ecosystem
   > - How to measure success
   > - How recruiting changes
   > - How fragile OSS communities and projects can benefit from more companies adopting the new approach
- 📊 [View slides](https://jlprat.github.io/3rd-party-outbound-ospo/index.html)
- 🎥 [Watch video](https://peertube.xwiki.com/w/jXnZBWsiZ4spFaD6aawxfw)

****

**Establishing metrics early as an integral part of your OSPO first steps** ­— March 18th 2022

- 🗣 Speaker: **Daniel Izquierdo** from **[Bitergia](https://bitergia.com/)**
- 📜 _Abstract_: 
   > There are several reasons to build an OSPO. If we think about the early days to start this journey, the OSPO manager is willing to effectively enable everyone in the company to effectively use and consume open source. However, there are missing points as undefined processes, cultural approach, understanding of how open source communities work, engagement, and others.
   > 
   > Whether you're consuming and/or producing open source, transparency and awareness are essential. Metrics and KPIs to measure direction and performance in your OSPO help increase that transparency in the process, and embedding them into your decision-making process help have more informed decisions.
- 📊 [View slides](/resources/onramp_20220318/20220318_OSPO_OnRamp_Establishing_metrics_OSPO_first_steps.pdf)
- 🎥 [Watch video](https://peertube.xwiki.com/w/dwt4WL7AXvBVnfyRgdfpaw)

****

**How to facilitate Open Source software licensing and get it right?** ­— February 18th 2022

- 🗣 Speaker: **Carsten Emde** from **[OSADL](https://www.osadl.org/)**

****

**Manage Log4Shell and other open-source vulnerabilities with Eclipse Steady** ­— January 14th 2022

- 🗣 Speaker: **Henrik Plate** from **[SAP](https://pages.community.sap.com/topics/open-source)**
- 📊 [View slides](/resources/onramp_20220114/20220114_OSPO_Alliance_Log4Shell.pdf).

****

**Intro into the OSPO OnRamp concept** ­— December 10th 2021

- 🗣 Speaker: **Michael Plagge** from the **[Eclipse Foundation](https://www.eclipse.org/)**
- 📊 [View slides](/resources/onramp_20211210/20211210_OSPO_OnRamp.pdf) - Call for active participation.

****

**The SAP OSPO** ­— December 10th 2021

- 🗣 Speaker: **Michael Picht** from **[SAP](https://pages.community.sap.com/topics/open-source)**
- 📊 [View slides](/resources/onramp_20211210/20211210_ext_ospo_onramp_sap_ospo_overview.pdf).

****

**The OW2 OSS Good Governance initiative, an OSPO implementation blueprint** ­— December 10th 2021

- 🗣 Speaker: **Cédric Thomas** from **[OW2](https://ow2.org)**
- 📊 [View slides](/resources/onramp_20211210/20211210_onramp-ggi-introduction-cedricthomas-CC-BY.pdf).
