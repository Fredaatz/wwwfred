---
title: "👋🏼 IAST Software Solutions joins the OSPO Alliance"
date: 2024-09-26T10:00:00+01:00
layout: "single"
---

The community of the OSPO Alliance welcomes a new member from the automotive industry: **IAST (Ingenieurgesellschaft für Automotiv Software- und System-Technik)**.

[IAST Software Solutions](https://iast-software.com/), founded in 2018, is home to 200+ passionate engineers with deep strengths in building solutions across System Engineering, AUTOSAR, Model Based Development, Functional Safety, Cyber Security, Verification and Validation, OEM Bootloaders and Cloud Native application development.

> We at IAST are excited to be a part of the OSPO alliance. It's like finding and contributing to a treasure trove of knowledge and tools for managing open-source software – exactly what we've been looking for! But more than that, we believe collaboration is the future of software development, especially when the automotive industry is going through a major transformation. By joining OSPO Alliance, we're not just improving our own practices; we're becoming part of a larger community that's shaping the future of technology. By teaming up with OSPO Alliance and other alliance members, we're hoping to make our open-source practices more professional and less of a roll of the dice. It's all about making things smoother and more predictable for us, our clients, and the wider open-source community.
> 
> As a company focused on the automotive industry, we're always pushing to stay ahead of the curve. The alliance's collaborative approach to finding the right tools and best practices fits perfectly with our goal of delivering cutting-edge, customer-focused solutions. It's like having a whole team of innovators working alongside us, turbocharging our ability to create and contribute to groundbreaking open-source projects.

Welcome IAST! We're glad to have you onboard 🙂

