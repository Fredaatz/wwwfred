---
title: "👋🏼 Open Source Experts join the OSPO Alliance as new member"
date: 2025-01-15T10:00:00+01:00
layout: "single"
---

Starting 2025, we are very excited to welcome **Open Source Experts** as **member of OSPO Alliance** ! 

**Open Source Experts**’s mission is to provide access to **Open Source expertise**, which is often dispersed among numerous specialized, smaller players, by serving as a single point of contact to meet customers’ needs. To accomplish this, they rely on a network of qualified partners. In addition to their founding partners (**Arawa, BlueMind, FactorFX, Inno3**, and **Worteks**), the initiative is also supported by organizations such as **Atos/Eviden, Dalibo, Eclipse Foundation, ITSM-NG, Ocaml PRO, Oslandia**, and **Softeam**. All of these organizations have been committed and active in the Open Source ecosystem for over **20** years, contributing significantly to multiple Open Source projects.

**Open Source Experts** sees immense value in the mission and objectives of the **OSPO Alliance**. They have long been committed to the adoption and development of Open Source technologies across public administrations and private enterprises. Through their partner network, they offer a comprehensive range of solutions to meet diverse needs while fostering innovation and community engagement. These principles align seamlessly with the goals of the **OSPO Alliance** in promoting **Open Source Program Offices (OSPOs)** as vital mechanisms for governance, strategy, and community collaboration.

> We are eager to contribute to the OSPO Alliance initiative by:
> 1.	Actively participating in discussions, workshops, and collaborative activities organized by the OSPO Alliance.
> 2.	Promoting the initiative within our partner network.


Welcome to the table **Open Source Experts** [opensource-experts.com](https://opensource-experts.com/)!!! 

