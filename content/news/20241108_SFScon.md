---
title: "🌍️ OSPO Alliance and GGI represented at the SFScon, November 8-9 in Bolzano, Italy."
date: 2024-10-10T10:00:00+01:00
layout: "news"
---

📢 Meet **RIOS** and the **OSPO Alliance** during **SFScon** and do not miss our talk about **OSPOs** by **Valentina Del Prete**.

🚀We are happy that the OSPO Alliance will be presented by our colleagues from RIOS (Rete Italiana Open Source) in Bolzano. Many thanks to **Valentina Del Prete** from **Seacom** who will give the talk: **How to set up an Open Source Program Office?** Get insights and use cases from the OSPO Alliance. 

If you are attending **SFScon**, please join us on Friday November 8 at 16h40! 

#SFScon #OpenSource #OSPOAlliance #Innovation #OW2

More information: [Track 2024 - Agenda details](https://www.sfscon.it/tracks/ow2-track-2024/).