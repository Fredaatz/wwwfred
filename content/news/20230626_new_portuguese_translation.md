---
title: "Portuguese translation of the GGI Handbook available!"
date: 2023-06-26T0:30:00+02:00
layout: "news"
---

The OSPO Alliance is proud to announce the general availability of the Portuguese translation of the GGI Handbook, version 1.1! Get it now -- free and open source as usual, from our website:

* [Good Governance Initiative Handbook v1.1, Portuguese translation](/docs/ggi_handbook_v1.1_pt.pdf).

The [GGI Handbook](/ggi/) is a community-led initiative, developed and translated thanks to the relentless efforts of our contributors. We are deeply grateful to all of them, and to the great work done by the Weblate team who offers free hosting to open source projects. Open source and collaboration to their best, delivering useful resources for the greater public, once again.

Check out all of our publications:

* [Official (english) release - v1.1](/docs/ggi_handbook_v1.1.pdf).
* Translations (v1.1):
  - [French translation - v1.1_fr](/docs/ggi_handbook_v1.1_fr.pdf),
  - [German translation - v1.1_de](/docs/ggi_handbook_v1.1_de.pdf).
