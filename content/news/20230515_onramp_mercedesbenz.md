---
title: "OSPO OnRamp May - A vision of FOSS at Mercedes-Benz TI"
date: 2023-05-15T10:30:00+02:00
layout: "news"
---

The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source.

This month, we will welcome Wolfgang Gehring who's FOSS Ambassador & OSPO Lead at Mercedes-Benz-TI. He'll tell the story of their Free and #OpenSource #Sofware (aka #FOSS) journey they started about 6 years ago.

📅 May 26, 2023
⏰ 10:30-12:00 am CEST
▶️ https://ospo.zone/onramp/

It's open to all with no registration in a safe environment. Just come and grab a seat.

At Mercedes-Benz, they decided that they don’t want to “just use FOSS”, but to fully embrace all aspects of it. This means they needed a dedicated Open (and Inner) Source strategy and an Open Source Program Office – but first, they had to define what it really means to “embrace FOSS”. Come with our speaker of the month, Wolfgang, on their journey which started about 6 years ago. He'll show you what we learned, what the crucial points were in implementing the strategy, and what they wish they had already known at the beginning.

![OnRamp SpeakerCard](/news/img/202305_OSPO_OnRamp_SpeakerCard_Wolfgang_Gehring.png)
