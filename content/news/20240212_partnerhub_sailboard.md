---
title: "Sailboard becomes a Trusted Partner!"
date: 2024-02-11T15:00:00+01:00
layout: "news"
---

<img src="/images/partner-logos/sailboard-logo.svg" alt="Sailboard logo" width="300px" style="float: right">

<br />

We are happy to announce our first Trusted Partner, [Sailboard](https://sailboard.io)! 

<br />

In the context of the OSPO Alliance, [Sailboard](https://sailboard.io) helps organisations implement and maintain Open Source Program Offices using a standard approach based on the OSS Good Governance Initiative blueprint.


Sailboard has been working with us along the last year or so, and made significant contributions to the Alliance by translating the [GGI handbook into Dutch](/ggi/). They are based in the Netherlands, providing consulting services to build an open ecosystem with open organizations, open leadership and better use cases of open source software, licenses and standards.


