---
title: "🇮🇹 New Italian translation for the GGI Handbook v1.2"
date: 2024-10-21T10:00:00+01:00
layout: "single"
---

The OSPO Alliance is delighted to announce a new translation of the [Good Governance Handbook v1.2 in Italian](/docs/ggi_handbook_v1.2_it.pdf)!

This has been -- again -- a beautiful community effort, with contributors kindly joining the effort on our translation platform, [Weblate](https://hosted.weblate.org/projects/ospo-zone-ggi). This new version brings updated resources and guidelines for the local deployment of OSPOs in Italian communities. We want to express our gratitude to all the good people, translators and reviewers, involved in this new translation. 

Please share it widely with your colleagues and network!
