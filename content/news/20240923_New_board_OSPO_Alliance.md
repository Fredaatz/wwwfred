---
title: "📣 New Board Election for the OSPO Alliance"
date: 2024-09-23T10:00:00+01:00
layout: "single"
---

A Brand New Reinforced Board for the OSPO Alliance: on September 23rd, the **OSPO Alliance** announced the election of a new augmented board: **Frédéric Aatz** as Chair, along with **Nicolas Toussaint** and **Florent Zara** as Vice-Chairs. These appointments mark a significant milestone for the Alliance, reinforcing its commitment to open source governance. With Frédéric Aatz’s experience at Microsoft and the contributions of Nicolas Toussaint and Florent Zara, the Alliance is poised to reach new heights. 🚀

Congratulations to all three! 🎉

We are really grateful to **Boris Baldassari** for his long tenure as Chair of the Alliance. He will of course stay with us and contribute to our task forces.

### About

**Frederic Aatz** (Chair) retired after 40 years in the industry, notably at Microsoft where he held various positions as Director of Open Source Strategy and Marketing & Business Lead for Microsoft Azure France, driving the launch and development of the France datacenter regions. Passionate about open source, management and organizations, he now dedicates part of his time as volunteer within the OSPO Alliance. [LinkedIn](https://www.linkedin.com/in/fredaatz/)

**Nicolas Toussaint** (Vice Chair) is responsible for the Open Source Program Office at Orange Business, now particularly interested in promoting open source and legal compliance challenges.
Initially an embedded software developer and architect, he has been working on open source products for 19 years and has been involved in open source compliance for 13 years at Orange and Orange Business. [LinkedIn](https://www.linkedin.com/in/nicolas-toussaint-9662787/)

**Florent Zara** (Vice Chair) has been active in the FLOSS community since 1999. He has 20 years of experience as an Open Source advisor, focusing on software quality, governance, licensing, Innersource, and change management. As Open Source Services Team Lead at the Eclipse Foundation, he supports Eclipse members and projects to better understand, manage and master Open Source. Florent is also a board member at LinuxFr.org, a prominent French-speaking site about Free and Open Source software. [LinkedIn](https://www.linkedin.com/in/florentzara/)