---
title: "Next OnRamp Meeting on February 17th"
date: 2023-02-17T10:30:00+02:00
layout: "news"
---

It's open to all with no registration in a safe environment. Just come and grab a seat.

📅 Feb 17th, 2023
⏰ 10:30-12:00 CET
▶️  https://ospo-alliance.org/onramp/

The Free Software Foundation Europe (FSFE) manages a Legal Network of experts in different fields involved in Free Software legal issues. With over 400 members from different legal systems, academic backgrounds, and affiliations around the world, its aim is to promote discussion and foster better knowledge of the legal constructs that back Free Software. This talk will discuss how the Legal Network has promoted and sped up recent developments in the legal field relating to Free Software, as well as touch on affiliated events that the FSFE organizes for the Network, including its annual conference.
 
![OnRamp SpeakerCard](/news/img/SocialCards_Gabriel-Ku-Wei-Bin.jpg)

