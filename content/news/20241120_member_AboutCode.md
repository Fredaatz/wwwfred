---
title: "👋🏼 AboutCode joins the OSPO Alliance"
date: 2024-11-04T10:00:00+01:00
layout: "single"
---

We are excited to share that **AboutCode** has joined the **OSPO Alliance**!

🌐 As a leader in promoting open source governance (OSS), **AboutCode** is dedicated to supporting **Open Source Program Offices (OSPO)**. Their mission is to help organizations adopt and manage open source software effectively, ensuring compliance and security.
 
By joining the OSPO Alliance, **AboutCode** aims to collaborate with other industry leaders to foster a more open and collaborative future. Their tools and resources are designed to empower organizations to navigate the complexities of open source, from licensing to community engagement. 

> Together, we are building a stronger, more transparent, and innovative ecosystem. Join us on this journey towards a more open world!

#OpenSource #OSS #OSPO #AboutCode

