---
title: "🤝 Eclipse Foundation becomes a Trusted Partner!"
date: 2024-10-23T13:00:00+02:00
layout: "news"
---

<img src="/images/partner-logos/eclipse_foundation_logo.png" alt="Eclipse Foundation logo" width="300px" style="float: right">

<br />

We are happy to announce a new Trusted Partner, the [Eclipse Foundation](https://www.eclipse.org)!

<br />

The [Eclipse Foundation](https://www.eclipse.org) is a founding member of the OSPO Alliance, still involved in its governance and actively participating from the start to all its initiatives : the GGI handbook and its translations, the OnRamp meetings, the outreach, … 

For a couple of years, the foundation has established a [range of services](https://www.eclipse.org/os4biz/services/) to support the adoption of open source — such as training programs, assistance in setting up an Open Source Program Office following the GGI methodology, offering a complete open source/InnerSource framework, and improving open source efficiency—to help organizations leverage open source and participate in the open source ecosystem. Services can be delivered remotely or in-person.

**About the Eclipse Foundation**

The Eclipse Foundation provides our global community of individuals and organisations with a business-friendly environment for open source software collaboration and innovation. We have a proven track record of enabling community-led and industry-ready open source innovation earned over two decades. We host the Eclipse IDE, Adoptium, Software Defined Vehicle, Jakarta EE, and over 430 open source projects, including runtimes, tools, specifications, and frameworks for cloud and edge applications, IoT, AI, automotive, systems engineering, open processor designs, and many others. Headquartered in Brussels, Belgium, the Eclipse Foundation is an international non-profit association supported by over 385 members. To learn more, visit us at [eclipse.org](https://www.eclipse.org) and follow us on social media [X](https://twitter.com/EclipseFdn), [LinkedIn](https://www.linkedin.com/company/34093/), [Mastodon](https://mastodon.opencloud.lu/@EclipseFdn@mastodon.social), [Youtube](https://www.youtube.com/@EclipseFdn).
