---
title: "OSPO OnRamp June - The Governance Game"
date: 2023-06-01T10:30:00+02:00
layout: "news"
---

This month's On-Ramp meeting will welcome Jan Ainali who's Codebase Steward at Foundation for Public Code. He'll present us the Governance Game and how your organisation can use it to create discussions about effective open source governance.

📅 June 16st, 2023
⏰ 10:30-12:00 am CEST
▶️ https://ospo.zone/onramp/

It's open to all with no registration in a safe environment. Just come and grab a seat.

The Governance game is a tool to get a conversation and reflection about governance in a codebase started. It can be used either just to learn about the complexities of collaboratively built codebases in general, or for a community of a specific codebase to get a shared understanding of what current challenges are and explore changes to mitigate them.

https://governancegame.publiccode.net/

![OnRamp SpeakerCard](/news/img/202306_OSPO_OnRamp_SpeakerCard_Jan_Ainali.png)
