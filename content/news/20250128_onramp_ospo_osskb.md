---
title: "Next OnRamp meeting on March 21st: OSSKB or the creation of SBOMs using open source tools"
date: 2025-01-28T13:00:00+01:00
layout: "news"
---

The replay of the OnRamp January session is online. In case you missed it, you can still watch our deep dive into the [Open Source AI Definition](/onramp/past_meetings/) with Stefano Maffulli, the Executive Director of the Open Source Initiative](https://www.opensource.org).

In March, Agustín Benito will continue our 2025 season of our OnRamp sessions with a more tooling-oriented topic. SBOMs, knowledge-base, SCA software, etc…

Save the date!

- 📅 Friday, March 21st
- ⏰ 10:30-12:00 CET
- 🎙️ **Agustín Benito Bethencourt** — Ecosystem manager at [SCANOSS](https://www.scanoss.com/) and Ecosystem Lead at the [Software Transparency Foundation](https://www.softwaretransparency.org/).
- 📜 **[osskb.org](http://osskb.org/) or how to create complete SBOMs using open source tools**.

As usual, it is an online event open to all with no registration in a safe environment. Just come and grab a seat. Please connect to our 🌐 [BigBlueButton instance](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join) on the given date and time.

> The Software Transparency Foundation provides a free service, osskb.org, for developers, open source projects, non-profits, and researchers to detect and analyse open source software within their compositions using SCA tools. It offers licensing information, identifies problematic code snippets, and integrates with various tools via an openAPI. Backed by SCANOSS’s OSS KB knowledge base, the service's open source code is available on GitHub for users to explore, build upon, or contribute to. This presentation will introduce the Foundation, showcase osskb.org and its integrations, and provide live demonstrations of its capabilities.

![OnRamp SpeakerCard](/news/img/202503_OSPO_OnRamp_SpeakerCard_Agustin_Benito.png)

You can import this specific session into your calendar with this [📅 ICS file](/resources/onramp/20250321_onramp_calendar.ics).

_The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. More on the [dedicated page](/onramp)._
