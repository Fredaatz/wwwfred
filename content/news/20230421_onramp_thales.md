---
title: "OSPO OnRamp - Thales OSPO feedback"
date: 2023-04-21T10:30:00+02:00
layout: "news"
---

The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source.

This month we will welcome Sébastien LEJEUNE who's OSS Advocate at Thales. He'll give us his **feedback on Open Source Software & Hardware governance in Thales**.

📅 April 21st, 2023
⏰ 10:30-12:00 am CEST
▶️  https://ospo-alliance.org/onramp/

It's open to all with no registration in a safe environment. Just come and grab a seat.

The OSPO Alliance is an open community that brings and shares actionable solutions to organisations willing to professionally manage the usage, contribution to and publication of open source software. The Good Governance Initiative (GGI) proposes a methodological framework to assess and improve open-source awareness, compliance and governance within organizations. The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. GGI and the OnRamp series are key initiatives from the OSPO Alliance.

![OnRamp SpeakerCard](/news/img/202304_OSPO_OnRamp_SpeakerCard_Sebastien_Lejeune.png)
