---
title: "👋🏼 City of Paris confirms support to the OSPO Alliance"
date: 2024-11-01T10:00:00+01:00
layout: "single"
---

We are very proud and honoured to have **City of Paris** joining as **member of OSPO Alliance** ! 

This is the achievement of a long term involvement as an active member of the **OSPO Alliance community**, early adopter of the **Good Governance Initiative** framework, contributing to a structured **Open Source Program Office (OSPO)** that coordinates its open source activities led by their **Open Source program Officer Philippe Bareille**. 

This **OSPO** plays a key role in managing and promoting free software within the municipal administration, ensuring transparency, digital sovereignty, and reducing development and maintenance costs., which aim to promote the development of free software and foster a dynamic community ecosystem.

**City of Paris** plays a pioneering role in the adoption and promotion of free and open source software. Since **2002**, it has implemented a free software policy centred around the **Lutece platform**, developed specifically for municipal administrations. This platform allows the City of Paris to offer dozens of free and open source applications to its employees and citizens. Also **CitéLibre**, designed for local authorities, allows the rapid deployment of digital services without requiring coding skills.

Welcome **City of Paris** [Paris Open Source](https://opensource.paris.fr/)! 🗼

