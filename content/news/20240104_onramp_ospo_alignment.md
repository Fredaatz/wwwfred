---
title: "Next OnRamp meeting on January 19th to align OSPO with or strategy"
date: 2024-01-04T15:00:00+01:00
layout: "news"
---

Welcome to 2024! Happy New Year to everyone. We wish you all the best for your OSPO projects 😀

In a couple of weeks, Tobie Langel will inaugurate the 2024 season of our OnRamp sessions, showing that alignment isn't just for AI. One needs to increase OPSO awareness and impact by aligning with the organization's mission, goals, and strategy.

Save the date!

- 📅 Friday, January 19th,
- ⏰ 10:30-12:00 CET
- 🎙️ **Tobie Langel** — Open ecosystem strategy consultant, [UnlockOpen](https://unlockopen.com/).
- 📜 **Now more than ever, OSPO alignment with org strategy is key**.

As usual, it is an online event open to all with no registration in a safe environment. Just come and grab a seat. Please connect to our 🌐 [BigBlueButton instance](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join) on the given date and time.

> 2023 has been brutal for the tech industry. Even more so for OSPOs, which suffered budget and headcount cuts, felt increased pressure around software supply chain security, and saw AI stealing the spotlight.
>
> The very tactical approach that worked when credit was cheap and money was flowing no longer cuts it. In today's environment, strategic alignment with organizational mission, goals, and strategy becomes essential to demonstrate value and get executive attention, buy-in, and support.
>
> Tobie Langel has been helping organizations think strategically about their open source involvement for close to a decade. He'll be sharing some of the frameworks he uses to align externally-facing teams with internal stakeholders and become more impactful both internally and externally as a result.

![OnRamp SpeakerCard](/news/img/202401_OSPO_OnRamp_SpeakerCard_Tobie_Langel.png)

_The OSPO OnRamp meeting series provides an open, neutral and friendly forum, low-threshold entry point to exchange and learn about the basics of how to set up an Open Source Program Office and get started with open source. More on the [dedicated page](/onramp)._
