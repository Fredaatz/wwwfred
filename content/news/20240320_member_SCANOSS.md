---
title: "SCANOSS joins the OSPO Alliance"
date: 2024-03-20T10:00:00+01:00
layout: "news"
---

S like spring ! But also S like Software Composition Analysis (SCA) therefore, we are happy to welcome SCANOSS as a new member joining OSPO Alliance's community!

**SCANOSS**: Open OSS Inventory & Intelligence for DevSecOps. Enhance license, security, quality, and provenance visibility & control. You can find an extended description on their website https://www.scanoss.com/.

> SCANOSS aims to standardize software composition and declaration to promote transparency, increasing efficiency and cost reduction, revolutionizing the SCA market. OSPO Alliance is the right forum for promoting such agreements across the open source industry. Julián Coccia, SCANOSS CTO.


