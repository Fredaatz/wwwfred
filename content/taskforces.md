---
title: "Task forces"
date: 2021-07-28T13:00:00+02:00
show_featured_footer: false
layout: "single"
---

There are several task forces inside the OSPO Alliance, each of them working on
a given matter. Active ones are the: 

- [GGI Handbook task force](#ggi-handbook-task-force)
- [Communication task force](#communication-task-force)


They meet in the following virtual room for video-conferences:
🎥 https://bbb.opencloud.lu/rooms/flo-jqs-nir-elb/join

_Please note that we have and enforce a [governance](/governance/) and a [code of conduct](/code_of_conduct/) to ensure a respectful, inclusive and welcoming community._

## GGI Handbook task force

This task force is working on future versions of the [good governance handbook](/ggi)
and the improvement of the My GGI board. Work is currently underway on version 1.2,
scheduled for autumn 2023.

- Leader(s): Boris Baldassari
- Scheduling: every 2 weeks on Tuesday 14:00 CEST
- Resources: There is a [dedicated collaborative pad](https://pad.castalia.camp/p/good-governance-handbook-taskforce-ox70p9g) 
  used by the taskforce for minutes and consolidating the work done before pushing
  changes to the handbook. Other pads exists, dedicated to specific subjects:
  - [Metrics on activities](https://pad.castalia.camp/p/metrics-on-activities-2a50l5j)
  - [Relations between activities](https://pad.castalia.camp/p/handbook-relationships-nm80ltv)
  - [Innersource](https://pad.castalia.camp/p/innersource-task-force-xam0pdd)

- Active participant:
  - Fréderic Aatz
  - Boris Baldassari
  - Silona Bonewald
  - Gerardo Lisboa
  - Sébastien Lejeune
  - Silvério Santos
  - Nicolas Toussaint
  - Igor Zubiaurre
  - Florent Zara

## Communication task force

It is focused on external communication, disseminating the work done at the OSPO Alliance through participation in events, social media management, brand management, media relations, etc. 

- Leader(s): Catherine Nuel
- Scheduling: every 2 weeks on Thursday at 14:00 CEST
- Resources: There is a
  - [collaborative pad](https://pad.castalia.camp/p/communication-task-force-veg0arf) for minutes taking,
  - [GitLab Project](https://gitlab.ow2.org/ggi/ggi-com-tf/) to coordinate the work done. It is mainly used to 
    - share and store files (logos, boilerplates, etc.)
    - work collaboratively on deliverables
    - keep track of tasks and their status using issues

- Active participant:
  - Fréderic Aatz
  - Valentina Del Prete
  - Silona Bonewald
  - Catherine Nuel
  - Antonio Conti
  - Florent Zara


## Previous Task forces <!-- omit from toc -->

The following task forces are no longer active. Their deliverables have been validated and committed. Thank you to all those who actively participated. 

- The **Governance** task force has established the internal [governance](/governance/) of the OSPO Alliance (not to be confused with the Good Governance Intiative),
- The **Consultancy** task force has set up the [Partners Hub](/partners/).
