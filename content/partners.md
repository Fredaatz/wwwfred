---
title: "Consultancy Hub"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

## Introduction

OSPO Alliance Consultancy Partner Hub is a list of companies and organisations that can provide consultancy services in relation to the Good Governance Initiative (GGI). Each partner has the opportunity to provide a logo, short description of the services they offer, and link to their website. This documentation describes how it works and how to get listed.

The OSPO Alliance does not endorse, or provide any guarantee regarding, any of the companies listed in the Consultancy Hub. Listing in the Consultancy Hub merely indicates that companies provide specific services and support to help organisations implement the GGI.

If you have any question or concern about a partner's commitment to the rules, please send an email to ospo-zone-team @ framalistes.org.


## Service providers

### Our trusted partners

| <div style="width:190px">Logo</div> | Description   | Service provided |
|-------------------------------------|---------------|------------------|
| [![Sailboard](/images/partner-logos/sailboard-logo.svg)](https://sailboard.io)  | <br />[Sailboard](https://sailboard.io) helps organisations implement and maintain Open Source Program Offices using a standard approach based on the OSS Good Governance Initiative blueprint. | <br /><span class="label service">Service</span> |
| [![Eclipse Foundation](/images/partner-logos/eclipse_foundation_logo_small.png)](https://www.eclipse.org)  | <br />The [Eclipse Foundation](https://www.eclipse.org) has established a [range of services](https://www.eclipse.org/os4biz/services/) to support the adoption of open source — such as training programs, assistance in setting up an Open Source Program Office following the GGI methodology, offering a complete open source/InnerSource framework, and improving open source efficiency—to help organizations leverage open source and participate in the open source ecosystem. | <br /><span class="label service">Service <br /> Training</span> |

### Services provided

Partners may provide two kinds of services around the OSPO Alliance: Consulting and Training.

Examples:
* Consulting services around OSPO Building through GGI Implementation
* Training on how to build your OSPO, or raising awareness around open source in the company.
* Help and support on the tools and resources recommended in the GGI Handbook.
* Consulting services and support on specific domains, e.g. compliance or legal advice.

Notes:
* In the context of the OSPO Alliance, Innersourcing is considered a sub-topic of OSPOs, and can be included in any offering.
* Partners have to be associated with at least one of the above-mentioned categories.


### Submitting your application

In order to qualify as a consultancy partner, candidates must:
* Be a supporter of the OSPO Alliance (see our [statement of support](https://www.ospo-alliance.org/join)).
* Be sponsored by one or more members.
* Agree with the [code of conduct](#partners_code_of_conduct)
* Provide a public track record of meaningful contributions to the OSPO Alliance or GGI: it *is* important to us to have partners who really know about the GGI. 

These eligibility criteria remain valid throughout the participation.

When submitting their application, partners need to provide a logo, the description and categories for the services they provide, a web link, and email contact.

After sponsorship from one or more members, the steering committee validates new partners, according to the voting rules defined in the governance chapter.

### Partners Code of Conduct

In the interest of fostering an open and welcoming environment, we pledge to make participation in our projects and our community a harassment-free and inclusive experience for everyone. On top of our general [Code of Conduct](https://gitlab.ow2.org/ggi/ggi/-/blob/main/CODE_OF_CONDUCT.md), partners of the OSPO Alliance are expected to behave in a fair, respectful and responsible manner, both towards the public and the peers. This includes:

* Show level-playing field respect and consideration to other partners listed
* Sharing constructive feedback, while refraining from pitching specific products, unless invited to do so.
* Have a responsibility and obligation to comply with the antitrust laws. These laws generally prohibit the following:
  * Price-Fixing
  * Agreements to Allocate Markets
  * Concerted Refusals to Deal
  * Exchange of competitively sensitive information

The Steering committee may decide to remove a consultancy partner based on evidence of misbehavior.

Consultancy partners may also be removed from the listing after six (6) months without active participation to the community as defined by the OSPO Alliance Governance and ultimately decided by the Steering Committee.
