---
title: "Sponsors"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

Our work is made possible thanks to our contributors and sponsors. To see the list of active contributors, please refer to [our GitLab repository](https://gitlab.ow2.org/ggi/ggi/-/blob/main/MAINTAINERS.md). Sponsors, on the other hand, contribute in-kind resources and infrastructure to support the initiative. 

* Mailing lists are provided by:
  - [Eclipse Foundation](https://eclipse.org) for the community-wide [ospo.zone communications](https://accounts.eclipse.org/mailing-list/ospo.zone).
  - [OW2](https://ow2.org) for the [Handbook and OSS Governance communications](https://mail.ow2.org/wws/info/ossgovernance).
* We use [Omniscloud](https://omniscloud.eu/)'s [Big Blue Button instance](https://bbb.opencloud.lu) for our calls.
* Our [GitLab projects](https://gitlab.ow2.org/ggi/) are hosted on the [OW2 forge](https://ow2.org).
* Our [website](https://gitlab.eclipse.org/eclipse/plato/www) is hosted by the [Eclipse Foundation](https://eclipse.org).

Thank you, sponsors!