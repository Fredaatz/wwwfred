---
title: "Governance"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

## Introduction

The OSPO Alliance was launched in June 2021 by European non-profit organisations — OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code — and concerned individuals to promote an approach to excellence in open source software management.

The OSPO Alliance aims to bring actionable guidance and solutions to all organisations willing to professionally manage the usage, contribution to and publication of open source software, regardless of their size, revenue model or whether public or private. In particular, it will help organisations make the best out of the inexorable advance of open source software in their information systems and processes. We will facilitate the discovery and implementation of tools and best practices when engaging with open source software.

By professionalizing the management of open source software, the OSPO Alliance will make engaging with open source software less risky and more predictable. It will lower barriers to the adoption of open source and will enable organisations to leverage it to enhance their digital sovereignty.

## Organisation

The OSPO Alliance is Not an established legal entity. It is composed of supporting organisations and a group of people working pro-bono to achieve the OSPO Alliance goals. The OSPO Alliance works as an open source project and is based on transparency, openness and meritocracy.

## Definitions

- **OSPO**: Open Source Program Office, an implementation of the open source governance inside an entity.
- **Founders**: the four entities that launched the OSPO Alliance: Eclipse Foundation, Foundation for Public Code, OpenForum Europe and OW2.
- **Supporter**: entities who signed the Statement of Support.
- **Sponsor**: entities helping the OSPO Alliance by providing resources or financial contributions.
- **Participant**: see Roles section.
- **Ambassador**: see Roles section.
- **Maintainer**: see Roles section.
- **Vice-Chairperson**: see Roles section.
- **Chairperson**: see Roles section.
- **Initiative**: a collaborative activity we started to promote Open Source management practices and share them with the rest of the world.
- **Task Force**: an internal group working on a specific topic.
- **Steering Committee**: governing body responsible for the strategic decisions of the OSPO Alliance, providing guidance, direction and control. It is composed of the Maintainers of the OSPO Alliance.

## Roles

The governance recognises the following roles for people in the OSPO Alliance community:

* Participants
    * Join calls
    * Do not have voting rights
* Ambassadors
    * Join calls
    * Have voting rights
    * Contribute by recruiting, disseminating and communicating
    * Are Nominated and elected by the Maintainers
    * Can organise formal votes
    * Have an obligation to participate in at least 2/3 (two-thirds) of the formal votes
* Maintainers
    * Are also Ambassadors
    * Maintain the infrastructure and tooling, and have privileged access (e.g. merge MRs)
    * Contribute by joining actions regularly, active contributions recognized by fellow Maintainers 
* Vice-Chairperson
    * Also Maintainer
    * Supports/seconds the Chair in all their roles and steps in to lead when the Chair is unavailable
* Chairperson
    * Also Maintainer
    * Elected by the Maintainers on an annual basis
    * Convenes general calls
    * Steps into disputes and ensures respect of the code of conduct and rules.

## Promotion and demotion rules

* Ambassadors
    * Need to be sponsored by a maintainer or ambassador
    * Need to have a public track record of communications.
    * Goes through a vote.

* Maintainers
    * Need to be sponsored by a maintainer.
    * Need to have a public track record of contributions on tools.
    * Goes through a vote.

* Chairperson and Vice-Chairperson
    * Must be a maintainer.
    * Elected by the maintainers and ambassadors on an annual basis.

People lose their role and/or privilege if they don't comply with the requirements for 6 months.

Promotions and demotions are ultimately validated through the decision process.

The list of maintainers can be found in the [MAINTAINERS.md file](https://gitlab.ow2.org/ggi/ggi/-/blob/main/MAINTAINERS.md) at the root of [our repository](https://gitlab.ow2.org/ggi/ggi).

The list of ambassadors can be found in the [AMBASSADORS.md file](https://gitlab.ow2.org/ggi/ggi/-/blob/main/AMBASSADORS.md) at the root of [our repository](https://gitlab.ow2.org/ggi/ggi).

## Decision Process

By default, every decision is taken by consensus.

If a consensus cannot be reached among the Maintainers, even with the participation of the Chairperson as a mediator, a formal vote is required.

A formal vote:
* must be announced at least 6 days in advance on the mailing-list to give the voters enough time to be informed. Emails with voting content must have a [VOTE] prefix in their subject
* closing date must be clearly announced
* wording should be neutral so as not to influence voters to ensure independence
* is done only after clearly describing the foreseeable effects to give insight into what is expected to happen and what risks we take
* requires a minimum participation of 50% of the Maintainers to achieve a quorum
* is done by replying to the mailing-list with a +1/0/-1 answer per option, with an optional comment
* final tally is made by summing all votes. If the result is positive, the decision is taken in favour of the proposal
* is validated during the following meeting, closing the vote
* result is communicated in meeting minutes and recorded in a decision log file

## IP and trademark policy

### Trademarks 

The "OSPO Alliance" name, logos and branding design are the property of the OSPO Alliance.
All the remaining names, trademarks and logos are the property of their respective owners.

### Intellectual Property & Licence

Unless explicitly stated otherwise, all content is copyrighted by the "OSPO Alliance and others" and published under the CC-BY-4.0 Creative Commons Attribution 4.0 International licence, as it allows wide dissemination.
Please refer to the [licence legal text](https://creativecommons.org/licenses/by/4.0/legalcode).

As with any open source project, contributors shall make sure they comply with the [Developer Certificate of Origin](https://developercertificate.org/) manifest. Please note however that, based on the good faith of participants, we do not enforce it in our repositories.

## New version of this document

This document is published under the CC-BY-4.0 licence. In order to avoid any inconsistency, all modifications to this charter have to go through a formal vote as defined by the decision process in order to be enforced in the OSPO Alliance community.

## Related documents 

See the following related documents:

* [Statement of support](https://ospo-alliance.org/docs/statement_of_support.pdf)
* [Code of conduct](https://gitlab.ow2.org/ggi/ggi/-/blob/main/CODE_OF_CONDUCT.md)
* [Contributing guidelines](https://gitlab.ow2.org/ggi/ggi/-/blob/main/CONTRIBUTING.md)
* [List of Maintainers](https://gitlab.ow2.org/ggi/ggi/-/blob/main/MAINTAINERS.md)
* [List of Task forces](https://gitlab.ow2.org/ggi/ggi/-/blob/main/TASKFORCES.md)


