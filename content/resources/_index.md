---
title: "OSPO Resources"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "layout"
---

This section contains resources and supporting material for the OSPO Alliance, sorted by type.
- [OSPO Stories](#ospo-stories)
- [Tools and Methods for OSPOs](#tools-and-methods-for-ospos)
- [Best Practices](#best-practices)
- [More about the Good Governance Initiative](#more-about-the-good-governance-initiative)
  - [Generic Presentations](#generic-presentations)
  - [More presentations \& videos](#more-presentations--videos)
- [Other friendly OSPO initiatives \& networks](#other-friendly-ospo-initiatives--networks)
- [Other curated resources](#other-curated-resources)

## OSPO Stories

Discover how others are doing with their OSPOs, from large organisations to the public sector.

- "**A Vision of FOSS @Mercedes-Benz**" by Wolfgang Gehring — FOSS ambassador and OSPO Leader (Mercedes-Benz Tech Innovation). Talk given in May 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20230526/20230526_OSPO_OnRamp_Mercedes-Benz_OSPO.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1685090362627) (Starts at 00:10:16).
- "**Return of experience on Open Source Software & Hardware governance in Thales**" by Sébastien Lejeune — OSS Community manager (Thales Group). Talk given in April 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20230421/20230421_OSPO_OnRamp_Thales_OSPO.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1682064908249) (Starts at 00:13:01).
- "**Paris Open Source — The City's commitment to Open Source and roadmap**" by Philippe Bareilles — Open Source Program Officer ([City of Paris](https://opensource.paris.fr/)). Talk given in March 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20230317/20230317_OSPO_OnRamp_Paris_OSPO.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1679045082451) (Starts at 00:08:59)
- "**MAIF: a singular open-source strategy**" by Frédéric Pied and François Desmier ([MAIF](https://maif.fr/)). Talk given in October 2022 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20221021/20221021_OSPO_OnRamp_MAIF.pdf)
- "**Setting up an OSPO at a power utility**" by Lucian Balea (RTE). Talk given in June 2022 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20220617/220617_OSPO_OnRamp_Building_an_OSPO_at_RTE.pdf)
  - [Watch video](https://peertube.xwiki.com/w/bDWqEvU5twosD2J1jpYj3f)
- "**The SAP OSPO**" by Michael Picht (SAP). Talk given in December 2021 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20211210/20211210_ext_ospo_onramp_sap_ospo_overview.pdf)
- "**OSPO in the Academic World**" by Anne-Marie Scott — Board Chair, [Apereo Foundation](https://www.apereo.org/). Talk given in October 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20231020/20231020_OSPO_OnRamp_OSPO_Academic_World.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1697790501307)

## Tools and Methods for OSPOs

- "**Introduction to the Governance game (to start the discussion about OSPOs)**" by Jan Ainali — Codebase Steward (Foundation for Public Code). Talk given in June 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20230616/20230616_OSPO_OnRamp_The_Governance_Game.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/video/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1686902873481) (Starts at 00:15:22)
- "**Hermine project: Open source code and data for legal compliance**" by Camille Moulin and Benjamin Jean ([InnoCube](https://innocube.org/)). Talk given in September 2022 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20220916/20220916_OSPO_OnRamp_Hermine_v03.pdf)
- "**How to facilitate Open Source software licensing and get it right?**" by Carsten Emde (OSADL). Talk given in February 2022 as part of the OnRamp session series.
  - [Download slides](/resources/onramp_20220218/20220218_OSPO_OnRamp_OSADL_How-to-facilitate-Open-Source-software-licensing-and-get-it-right.pdf)
  - [Watch video](https://bbb.osadl.org/playback/presentation/2.3/d0de8b8cf7435dfa4ec3a9bbea43caea79fba6b6-1645177848413)
- "**FSFE’s Legal Network and its Legal and Licensing Workshop (LLW)**" by Gabriel Ku Wei Bin — Legal Coordinator ([FSFE](https://fsfe.org/)). Talk given in February 2023 as part of the OnRamp session series. 
  - [View slides](/resources/onramp_20230217/20230217_OSPO_OnRamp_FSFE_Legal_Network.pdf)
  - [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1676624644585)
- "**Market Readiness Level: how to select the right open source solution?**" by Antoine Mottier — CTO at [OW2](https://www.ow2.org/). Talk given in April 2024 as part of the OnRamp session series. 
    - 📊 [View slides](/resources/onramp_2024/20240419_OSPO_OnRamp_MRL.pdf)
    - 🎥 [Watch video](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1713514474233) 

## Best Practices

Metrics, Security, … topics of high interest to OSPOs

- "**Establishing metrics early as an integral part of your OSPO first steps**" by Daniel Izquierdo (Bitergia). Talk given in March 2022 as part of the OnRamp session series.
  - [Download slides](/resources/onramp_20220318/20220318_OSPO_OnRamp_Establishing_metrics_OSPO_first_steps.pdf)
  - [Watch video](https://peertube.xwiki.com/w/dwt4WL7AXvBVnfyRgdfpaw)
- "**Open Source Software Supply Chain Security — Why does it matter?**" by Mikael Barbero - Head of security - ([Eclipse Foundation](https://www.eclipse.org)). Talk given in January 2023 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20221216/221216_OSPO_OnRamp_State_of_the_Alliance.pdf)
  - [Watch video](https://peertube.xwiki.com/w/3rcCUc4Zs4ZD2Dmii5v8rC)
- "**Manage Log4Shell and other open-source vulnerabilities with Eclipse Steady**" by Henrik Plate (SAP Security Research). Talk given in January 2022 as part of the OnRamp session series.
  - [View slides](/resources/onramp_20220114/20220114_OSPO_Alliance_Log4Shell.pdf)

## More about the Good Governance Initiative

### Generic Presentations
  - [What’s new in version 1.2 of the Good Governance Initiative?](https://bbb.opencloud.lu/playback/presentation/2.3/e1d2665ed6d4ad3a07f23e5a3638fe49dc3a95b8-1700211741467) (starts at 8'20"). [Downlad slides](/resources/onramp_20231117/20231117_OSPO_OnRamp_GGI-1.2.pdf).
  - [GGI - Official presentation slidedeck](/docs/OSPO_Alliance_GGI_Presentation.pdf) - Last updated 2022-12 - 22 slides
  - [GGI - An OSPO implementation blueprint](https://www.ow2.org/download/OSS_Governance/WebHome/2111-GGi-Introduction-CedricThomas.pdf?rev=1.1) - Last updated 2021-11 - 32 slides
  - [GGI - OSS governance with a European perspective](https://www.ow2.org/download/Events/FOSDEM_2021/2102-FOSDEM-OW2-GGI-Intro.pdf) - last updated 2021-02 - 15 slides

### More presentations & videos
  - [EclipseCon Europe 2022](https://www.youtube.com/watch?v=703s2DN6oMU) Practical wisdom to build your OSPO: The Good Governance Initiative (42 min) - [Download slides](/docs/ECE%202022%20-%20OSPOs%20&%20Metrics.pdf)
  - [OW2Con 2022](https://www.youtube.com/watch?v=abuS9ORHGDA&t=12335s) [Roundtable] Open Source Program Offices and the Good Governance Initiative (GGI)
  - [FOSDEM 2021](https://www.youtube.com/watch?v=abQzK8gkZJU) (5 min)
  - [BlueHats Workshop 2021](https://www.dailymotion.com/video/x82vcud) (1 hour, French)
  - [Rete Italiana Open Source Week 2021](https://www.youtube.com/embed/I8ZZ52btxyY?hd=1&vq=hd1080&rel=0) (23 min)

## Other friendly OSPO initiatives & networks

- [BlueHats](https://www.modernisation.gouv.fr/communautes/blue-hats): French Open Source Community for civil servants
- [EC OSPO](https://interoperable-europe.ec.europa.eu/collection/ec-ospo): The European Commission Open Source Programme Office (EC OSPO) acts as a facilitator for activities outlined in the [EC Open Source strategy]([Open source software strategy](https://commission.europa.eu/about-european-commission/departments-and-executive-agencies/digital-services/open-source-software-strategy_en)).

## Other curated resources

This section lists relevant resources submitted by our members.

* A collection of best practices to make a codebase ready for collaboration, provided by the Foundation for Public Code: https://standard.publiccode.net/
* A paper published by [OpenForumEurope](https://openforumeurope.org) at the Digital Assembly, an event co-hosted by the French Government and the European Commission, on the 20th of June 2022:
  - [The OSPO: A New Tool for Digital Government](https://openforumeurope.org/publications/the-ospo-a-new-tool-for-digital-government/) (CC-BY-SA).
* A series of articles about SAP's corporate OSPO, by Michael Picht:
  - [Managing Open Source Software with an Open Source Program Office](https://blogs.sap.com/2021/09/23/managing-open-source-software-with-an-open-source-program-office/),
  - [How SAP Manages Open Source Software with an Open Source Program Office](https://blogs.sap.com/2021/10/28/how-sap-manages-open-source-software-with-an-open-source-program-office/).
