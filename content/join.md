---
title: "Join the Alliance"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

One of the objectives of the OSPO Alliance is to provide a low-barrier entry point for everything OSPOs. As a result, you only have to sign and [send us](mailto:ospo-zone-team@framalistes.org) this <a href="/docs/statement_of_support.pdf" target="_blank">statement of support</a> to become a member. There is no strong commitment.

Please provide us with:
* The signed statement of support.
* A logo that we can use in our Members page to list your organisation.
* A URL pointing to your preferred page demonstrating your engagement with OSPOs or open source.
* A brief description about your identity and motivation for joining, if you are interested in being advertised in our communications.
