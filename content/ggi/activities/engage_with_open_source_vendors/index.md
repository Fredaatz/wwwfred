---
title: "GGI Activity: Engage with open source vendors"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Engage with open source vendors

Activity ID: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_33.md).

## Description

Secure contracts with open source vendors that provide software critical to you. Companies and entities that produce open source software need to thrive to provide maintenance and development of new features. Their specific expertise is required on the project, and the community of users relies on their continued business and contributions.

Engaging with open source vendors takes several forms:

- Subscribing support plans.
- Contracting local service companies.
- Sponsoring developments.
- Paying for a commercial licence.

This activity implies considering open source projects as fully-featured products worth paying for, much like any proprietary products -- although usually far less expensive.

## Opportunity Assessment

The objective of this activity is to ensure professional support of open source software used in the organisation. It has several benefits:

- Continuity of service through timely bug fixes.
- Service performance through optimised installation.
- Clarification of the legal/commercial status of the software used.
- Access to early information.
- Stable budget forecast.

The cost is obviously that of the support plans selected. Another cost might be to depart from bulk outsourcing to large systems integrators in favour of fine-grained contracting with expert SMEs.

## Progress Assessment

The following **verification points** demonstrate progress in this activity:

- [ ] Open source used in the organisation is backed by commercial support.
- [ ] Support plans for some open source projects have been contracted.
- [ ] Cost of open source support plans is a legitimate entry in the IT budget.

## Recommendations

- Whenever possible, find local expert SMEs.
- Beware of large systems integrators reselling third-party expertise (reselling support plans that expert open source SMEs actually provide).

## Resources

A couple of links illustrating the commercial reality of open source software:

- [A quick read to understand commercial open source](https://www.webiny.com/blog/what-is-commercial-open-source).
