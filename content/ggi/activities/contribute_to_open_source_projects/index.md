---
title: "GGI Activity: Contribute to open source projects"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Contribute to open source projects

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_26.md).

## Description

Contributing to open source projects that are freely used is one of the key principles of good governance. The point is to avoid being a simple passive consumer and give back to the projects. When people add a feature or fix a bug for their own purpose, they should make it generic enough to contribute to the project. Developers must be allowed time for contributions.

This activity covers the following scope:

- Working with upstream open source projects.
- Reporting bugs and feature requests.
- Contributing code and bug fixes.
- Participating in community mailing lists.
- Sharing experience.

## Opportunity Assessment

The main benefits of this activity are:

- It increases the general knowledge and commitment to open source within the company, as people start contributing and get involved in open source projects. They get a feeling of public utility and improve their personal reputation.
- The company increases its visibility and reputation as contributions make their way through the contributed project. This shows that the company is actually involved in open source, contributes back, and promotes fairness and transparency.

## Progress Assessment

The following **verification points** demonstrate progress in this Activity:

- [ ] There is a clear and official path for people willing to contribute.
- [ ] Developers are encouraged to contribute back to open source projects they use.
- [ ] A process is in place to ensure legal compliance and security of contributions by developers.
- [ ] KPI: Volume of external contributions (code, mailing lists, issues..) by individual, team, or entity.

## Tools

It may be useful to follow contributions, both to keep track of what is contributed and to be able to communicate on the company's effort. Dashboards and activity tracking software can be used for this purpose. Check:

- Bitergia's [GrimoireLab](https://chaoss.github.io/grimoirelab/)
- [ScanCode](https://scancode-toolkit.readthedocs.io)

## Recommendations

Encourage people within the entity to contribute to external projects, by:

- Allowing them time to write generic, well-tested bug fixes and features, and to contribute them back to the community.
- Providing training to people about contributing back to open source communities. This is both about technical skills (improving your team's knowledge) and community (belonging to the open source communities, code of conduct, etc.).
- Provide training on legal, IP, technical issues, and set up a contact within the company to help with these topics if people have doubts.
- Provide incentives for published work.
- Note that contributions from the company/entity will reflect its code quality and involvement, so make sure your development team provides code that is good enough.

## Resources

- The [CHAOSS](https://chaoss.community/) initiative from the Linux Foundation has some tools and pointers about how to track contributions in development.

## Proposed next activities

- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Now that there is some publicly visible contribution and commitment from the organisation, start communicating about it!
- [GGI-A-24 - Manage key indicators](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Make contribution to OSS projects visible and measurable. This will help with the dissemination of the initiative and uplift people's moral.
- [GGI-A-27 - Belong to the open source community](https://ospo-alliance.org/ggi/activities/belong_to_the_open_source_community) Contributing to the OSS community is the first step to becoming part of it. Once people start contributing, they become more involved in the project's health and governance and can eventually become maintainers, ensuring a sustainable and healty project and roadmap.
- [GGI-A-29 - Engage with open source projects](https://ospo-alliance.org/ggi/activities/engage_with_open_source_projects) Open source projects value meritocracy. Now that you have demonstrated a good understanding of the code and processes, you can get involved in the project and make your contributions more official.
- [GGI-A-36 - Open source enabling innovation](https://ospo-alliance.org/ggi/activities/open_source_enabling_innovation) Contributing to OSS projects and interacting with external contributors is an enabler to foster innovation.
- [GGI-A-39 - Upstream first](https://ospo-alliance.org/ggi/activities/upstream_first) Contributing to OSS projects really makes sense if the updates are made available in the upstream project, on a regular and institutionalised basis.
