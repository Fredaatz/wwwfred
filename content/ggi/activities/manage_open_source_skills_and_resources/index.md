---
title: "GGI Activity: Manage open source skills and resources"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Manage open source skills and resources

Activity ID: [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

## Description

This activity is focused on **software development** skills and resources. It includes the technologies and specific development skills of developers, as well as the overall development processes, methods and tools.

A vast amount of documentation, forums and discussions stemming from the ecosystem, and public resources is available for open source technologies. In order to fully benefit from their open source approach, one has to establish a roadmap of its current assets and desired targets to set up a consistent program for development skills, methods and tools within the teams.

**Domains of application**

One needs to establish the domains where the program will be applied, and how it will improve the quality and efficiency of code and practices. As an example, the program won't have the same benefits if there is only a single developer working on open source components, or if the whole development life cycle is optimised to include open source best practices.

One needs to define the scope to be embraced for open source development: technical components, applications, modernising or creating new development. Examples of development practices that can benefit from open source are:

- Cloud administration.
- Cloud-native applications, how to innovate with these technologies.
- DevOps, Continuous Integration / Continuous Delivery.

**Categories**

- Skills and resources required to develop open source software: IP, licensing, practices.
- Skills and resources required to develop software using open source components, languages, technologies.
- Skills and resources required to use open source methods and processes.

## Opportunity Assessment

Open source tools are increasingly popular among developers. This Activity addresses the need to avoid the proliferation of heterogeneous tools within a development team. It helps define a policy in this domain. It helps optimise training and experience building. A skills inventory is used for recruitment, training and succession planning in case a key employee leaves the company.

We would need a methodology to map open source software development skills.

## Progress Assessment

The following **verification points** demonstrate progress in this Activity:

- [ ] There is a description of the open source production chain (the "software supply chain"),
- [ ] There is a plan (or a wish list) for the rationalisation of development resources,
- [ ] There is a skills inventory summarising current developers' skills, education, and experience,
- [ ] There is a training wish list and program dealing with skills gaps,
- [ ] There is a list of missing open source development best practices and a plan to apply them.

## Recommendations

- Start simple, grow the analysis and roadmap steadily.
- When recruiting, set a strong emphasis on open source skills and experience. It's always easier when people already have an open source DNA than training and coaching people.
- Check training programs from software vendors and open source schools.

## Resources

More information:

- An introduction to [what is a Skills Inventory?](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) from Robert Tanner.
- An article about open source skills: [5 Open Source Skills to Up Your Game and Your Resume](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

This activity can include technical resources and skills such as:

- **Popular languages** (such as Java, PHP, Perl, Python).
- **Open source frameworks** (Spring, AngularJS, Symfony) and testing tools.
- Agile, DevOps and open source **development methods and best practices**.

## Proposed next activities

- [GGI-A-28 - Human Resources perspective](https://ospo-alliance.org/ggi/activities/hr_perspective) Once open source resources have been identified internally to help with open source awareness, make the Human Resources department value them as well, for both existing and future employees.

