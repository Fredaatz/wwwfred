---
title: "GGI Activity: Manage legal compliance"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Manage legal compliance

Activity ID: [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_21.md).

## Description

Organisations need to implement a legal compliance process to secure their usage and participation in open source projects.

Mature and professional management of legal compliance, in the organisation and across the supply chain, is about:

- Performing a thorough analysis of the intellectual property that includes licence identification and compatibility checking.
- Ensuring the organisation can safely use, integrate, modify and redistribute open source components as part of its products or services.
- Providing employees and contractors with a transparent process about how to create and contribute to open source software.

_Software Composition Analysis (SCA)_:  A significant part of legal and IP issues result from the usage of components released under licences that are either incompatible between them or incompatible with the way the organisation wants to use and redistribute the components. SCA is the first step in sorting out those issues as "you need to know the problem to fix it". The process is to identify all the components involved in a project in a Bill of Material document, including build and test dependencies.

_Licence checking_: A licence checking process uses a tool to automatically analyse the code base and identify licences and copyrights within. If executed regularly and ideally integrated into continuous build and integration chains, this allows catching IP issues early.

## Opportunity Assessment

With the ever-growing use of OSS in an organisation's information systems, it is essential to assess and manage potential legal exposure.

However, checking licences and copyrights can be tricky and costly. Developers need to be able to check IP and legal questions quickly.
Having a team and a corporate officer dedicated to IP and legal questions ensures proactive and consistent management of legal questions, helps secure open source components' usage and contributions and provides a clear strategic vision.

## Progress Assessment

The following **verification points** demonstrate progress in this Activity:

- [ ] There is an easy-to-use Licence checking process available for projects.
- [ ] There is an easy-to-use IP checking process available for projects.
- [ ] There is a team or person responsible for legal compliance within the organisation.
- [ ] Regular audits to assess legal compliance are scheduled.

Other ways to set up verification points:

- [ ] There is an easy-to-use licence checking process.
- [ ] There is an easy-to-use legal/IP team.
- [ ] All projects provide the required information for people to use and contribute to the project.
- [ ] There is a contact in the team for questions related to IP and licencing.
- [ ] There is a corporate officer dedicated to IP and licencing.
- [ ] There is a dedicated team for questions related to IP and licencing.

## Tools

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossology](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

## Recommendations

- Inform people about the risks associated with licensing in conflict with business goals.
- Propose an easy solution for projects to set up licence checking on their codebase.
- Communicate on its importance and help projects to add it to their CI systems.
- Provide a template or official guidelines for project structure.
- Set up automated checks to make sure that all projects comply with the guidelines.
- Consider conducting an internal audit to identify licences of the company infrastructure.
- Provide basic IP and licensing training for at least one person per team.
- Provide complete IP and licencing training for the officer.
- Set up a process to escalate IP and licencing issues to the officer.

Remember that compliance is not just about legal; it's also about IP. So here are a few questions to help understand the consequences of legal compliance:

- If I distribute an open source component and do not respect the licence conditions, I infringe the licence --> legal implications.
- If I use an open source component within a project that I wish to distribute/publish, that licence may oblige visibility on elements of code that I do not want to make open source --> Confidentiality impact for my company's tactical advantage and with 3rd parties (legal implications).
- It is an open discussion about whether using an open source licence for a project I want to publish grants relevant IP --> IP implications.
- If I make a project open source _before_ any patent process, that _probably_ excludes the creation of patents concerning the project --> IP implications.
- If I make a project open source _after_ any patent process, that _probably_ allows the creation of (defensive) patents concerning that project --> IP potential.
- In complex projects that bring in many components with many dependencies, the multitude of open source licences may exhibit incompatibilities between licences --> legal implications (cf. activity GGI-A-23 - Manage software dependencies).

## Resources

- There is an extensive list of tools on the [Existing OSS compliance group page](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Recommended Open Source Compliance Practices for the enterprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). A book by Ibrahim Haddad, from the Linux Foundation, about open source compliance practices for the enterprise.
 [OpenChain Project](https://www.openchainproject.org/)

## Proposed next activities

- [GGI-A-24 - Manage key indicators](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Make legal compliance concerns, processes and results visible and measurable. This will help people realise its importance earlier in the process.

