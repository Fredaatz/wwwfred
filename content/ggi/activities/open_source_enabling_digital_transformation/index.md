---
title: "GGI Activity: Open source enabling digital transformation"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Open source enabling digital transformation

Activity ID: [GGI-A-37](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_37.md).

## Description

> "Digital Transformation is the adoption of digital technology to transform services or businesses, through replacing non-digital or manual processes with digital processes or replacing older digital technology with newer digital technology." (Wikipedia)

When the most advanced organisations in Digital Transformation jointly drive change through their Business, IT and Finance to anchor digital in the way, they reconsider:

- Business model: value chain with ecosystems, as a service, SaaS.
- Finance: opex/capex, people, outsourcing.
- IT: innovation, legacy/asset modernization.

Open source is at the heart of digital transformation:

- Technologies, Agile practices, product management.
- People: collaboration, open communication, development/decision cycle.
- Business models: try & buy, open innovation.

In terms of competitiveness, the most visible processes are probably the processes that directly impact the customer experience.  And we have to recognize that the big players, as well as start-up companies, by delivering totally unprecedented customer experience, drastically changed customer expectations.

Customer experience as well as all the other processes within a company entirely depend on IT.  Every company has to transform its IT, this is what the digital transformation is about.  Companies that have not done it yet, have now to achieve their digital transformation as fast as possible, otherwise the risk is that they could be wiped out of the market.  Digital Transformation is a condition for survival.  Since the stakes are so high, a company cannot entirely leave the digital transformation to a supplier.  Every company has to get hands on with its IT, which means that every company has to get hands on with open source software because there is no IT without open source software.

Expected benefits of the digital transformation include:

- Simplify, automate core processes, make them real-time.
- Enable fast responses to competitive changes.
- Take advantage of Artificial intelligence and big data.

## Opportunity Assessment

Digital transformation could be managed by:

- Segments of the IT: Production IT, Business Support IT (CRM, billing, procurement…), Support IT (HR, Finance, accounting...), Big Data.
- Type of technology or process supporting the IT: Infrastructure (cloud), Artificial Intelligence, Processes (Make-or-Buy, DevSecOps, SaaS).

Injecting open source in a particular segment or technology of your IT reveals that you want to get hands on in this segment or technology, because you assessed that this particular segment or technology of your IT is important for the competitiveness of your company.
It is important to assess the position of your company compared not only with your competitors, but also with other industries, and key players in terms of customer experience and market solutions.

## Progress Assessment

- [ ] Level 1: Situation assessment

   I have identified:
   - the segments of IT that are important for the competitiveness of my company, and
   - the open source technologies required to develop applications in these segments.

   And I have thus decided:
   - on which segments I want to manage in-house the development of projects, and
   - on which open source technologies I need to build in-house expertise.

- [ ] Level 2: Engagement

   On some selected open source technologies used within the company, several developers have been trained and are recognized as valuable contributors by the open source community.  
   In some selected segments, projects based upon open source technologies have been launched.

- [ ] Level 3: Generalisation

   For all projects, an open source alternative is systematically being investigated during the inception stage of the project. To make it easier for the project team to study such open source alternative, a central budget, and a central team of architects, hosted in the IT Department, is dedicated to providing assistance to the projects.

**KPIs**:

- KPI 1. Ratio for which an open source alternative was investigated: (Number of projects / Total number of projects).
- KPI 2. Ratio for which the open source alternative was chosen: (Number of projects / Total number of projects).

## Recommendations

Beyond the headline, Digital Transformation is a mindset that involves some fundamental changes, and this should also (or even mainly) come from the top-level layers of the organisation. Management shall promote initiatives, new ideas, manage risks, and potentially update existing procedures to make them fit new concepts.

Passion is a huge factor of success. One of the means developed by key players in the field is to set up open spaces for new ideas, where people can submit, and freely work on, their ideas about digital transformation. Management should encourage such initiatives.

## Resources

- [Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
