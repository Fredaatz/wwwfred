---
title: "GGI Activity: Run code reviews"
date: 2021-10-07T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

----

**Table of contents**

{{< table_of_contents >}}

----


## Run code reviews

Activity ID: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_44.md).

## Description

Code review is a routine task involving manual and/or automated review of an application's source code before releasing a product or delivering a project to the customer. In the case of open-source software, code review is more than just about catching errors opportunistically; it is an integrated approach to collaborative development carried out at the team level.

Code reviews should apply to code developed in-house as well as to code reused from external sources, as it improves general confidence in code and reinforces ownership. It is also an excellent way to enhance global skills and knowledge within the team and foster team collaboration.

## Opportunity Assessment

Code reviews are valuable whenever the organisation develops software or reuses external pieces of software.
While being a standard step in the software engineering process, code reviews in the context of open-source bring specific benefits such as:

- When publishing internal source code, verify adequate quality guidelines are respected.
- When contributing to an existing open source project, verify that guidelines of the targeted project are respected.
- The publicly available documentation is updated accordingly.

It is also an excellent opportunity to share and enforce some of your company legal compliance policy rules, such as:

- Never remove existing licence headers or copyrights found in reused open-source code.
- Do not copy & paste source code from Stack Overflow without prior permission from the legal team.
- Include the correct copyright line when required.

Code reviews will bring trust and confidence to code. If people are not sure about the quality or potential risks of using a software product, they should conduct peer- and code- reviews.

## Progress Assessment

The following **verification points** demonstrate progress in this Activity:

- [ ] Open source code review is recognised as a necessary step.
- [ ] Open source code reviews are planned (either regularly or at critical moments).
- [ ] A process for conducting open-source code reviews has been collectively defined and accepted.
- [ ] Open-source code reviews are a standard part of the development process.

## Recommendations

- Code review is a collective task that works better in a good collaborative environment.
- Do not hesitate to use existing tools and patterns from the open-source world, where code reviews have been a standard for years (decades).

## Resources

- [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): a didactic read on code review found on Red Hat's Open Practice Library.
- [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): another interesting perspective on what code review is about.

## Proposed next activities

- [GGI-A-26 - Contribute to open source projects](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_projects) Code review is common practice in open source projects, for it improves code quality and knowledge sharing. Contributors performing code reviews usually feel more comfortable with external contributions and collaboration.

