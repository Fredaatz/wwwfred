---
title: "The Good Governance Handbook"
date: 2021-11-04T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The OSPO Alliance develops and maintains the OSS Good Governance Initiative (or GGI) blueprint developed by European open source organisations to help implement corporate-wide open source policies, and set up OSPOs. The methodology proposes a comprehensive approach based on five objectives (Goals) and a number of tasks (Activities) describing what steps should be implemented to build a successful OSPO.

The first release of the GGI Handbook has been published the 9th of October, 2021. The v1.1 update, published on the 8th of November 2022, introduces [new translations of the handbook](/ggi/introduction/#translations) and a [quick-and-easy deployment feature](/ggi/methodology/#automatic-setup-using-the-ggi-deployment-feature) to help organisations setup the program. The latest release, the v1.2, has been published on the 16th of October 2023. It features various updates to activities, new references, adds a section about innersource, and now suggests other activities when one is completed.

The latest PDF versions can be downloaded from here:
* [Official (english) release - v1.2](/docs/ggi_handbook_v1.2.pdf). 
* Translations (v1.1/v1.2):
  - [French translation - v1.2_fr](/docs/ggi_handbook_v1.2_fr.pdf),
  - [German translation - v1.2_de](/docs/ggi_handbook_v1.2_de.pdf),
  - [Portuguese translation - v1.1_pt](/docs/ggi_handbook_v1.1_pt.pdf).
  - [Dutch translation - v1.2_nl](/docs/ggi_handbook_v1.2_nl.pdf).
  - [Italian translation - v1.2_it](/docs/ggi_handbook_v1.2_it.pdf).
  - [Spanish translation - v1.2_es](/docs/ggi_handbook_v1.2_es.pdf).

You can also order printed versions of the GGI Handbook v1.0 [here](https://www.lulu.com/search?page=1&q=OW2).

### Table of content

* [Introduction](/ggi/introduction)
* [Organisation](/ggi/organisation)
* [InnerSource](/ggi/innersource)
* [Methodology](/ggi/methodology)
* Usage goal activities
  - [Inventory of open source skills and resources](/ggi/activities/inventory_of_open_source_skills_and_resources)
  - [Open source competency growth](/ggi/activities/open_source_competency_growth)
  - [Open source supervision](/ggi/activities/open_source_supervision)
  - [Open source enterprise software](/ggi/activities/open_source_enterprise_software)
  - [Manage open source skills and resources](/ggi/activities/manage_open_source_skills_and_resources)
* Trust goal activities
  - [Manage legal compliance](/ggi/activities/manage_legal_compliance)
  - [Manage software vulnerabilities](/ggi/activities/manage_software_vulnerabilities)
  - [Manage software dependencies](/ggi/activities/manage_software_dependencies)
  - [Manage key indicators](/ggi/activities/manage_key_indicators)
  - [Run code reviews](/ggi/activities/run_code_reviews)
* Culture goal activities
  - [Promote open source development best practices](/ggi/activities/promote_open_source_development_best_practices)
  - [Contribute to open source projects](/ggi/activities/contribute_to_open_source_projects)
  - [Belong to the open source community](/ggi/activities/belong_to_the_open_source_community)
  - [Human Resources perspective](/ggi/activities/human_resources_perspective)
  - [Upstream first](/ggi/activities/upstream_first)
* Engagement goal activities
  - [Engage with open source projects](/ggi/activities/engage_with_open_source_projects)
  - [Support open source communities](/ggi/activities/support_open_source_communities)
  - [Publicly assert use of open source](/ggi/activities/publicly_assert_use_of_open_source)
  - [Engage with open source vendors](/ggi/activities/engage_with_open_source_vendors)
  - [Open source procurement policy](/ggi/activities/open_source_procurement_policy)
* Strategy goal activities
  - [Setup a strategy for corporate open source governance](/ggi/activities/setup_a_strategy_for_corporate_open_source_governance)
  - [C-Level awareness](/ggi/activities/c-level_awareness)
  - [Open source and digital sovereignty](/ggi/activities/open_source_and_digital_sovereignty)
  - [Open source enabling innovation](/ggi/activities/open_source_enabling_innovation)
  - [Open source enabling digital transformation](/ggi/activities/open_source_enabling_digital_transformation)
* [Conclusion](/ggi/conclusion)

## Licencing

The GGI Handbook is published under the [CC-BY licence 4.0](https://creativecommons.org/licenses/by/4.0/).
